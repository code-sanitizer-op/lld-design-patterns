package com.lld.creational.builder;

import java.time.Year;

public class TestBuilderDesignPattern {

    Book.Builder builder = new Book.Builder("isbn1", "My-Book-1").setAuthor("Ash").setDescription("good book")
            .setPublished(Year.of(2022));

    Book book = builder.build();


}
